function nhanVien(taiKhoan,hoTen,email,matKhau,ngayLam,luongCoBan,chucVu,gioLam){
    this.taiKhoan = taiKhoan;
    this.hoTen = hoTen;
    this.email = email;
    this.matKhau = matKhau;
    this.ngayLam = ngayLam;
    this.luongCoBan = luongCoBan;
    this.chucVu = chucVu;
    this.gioLam = gioLam;
    this.tinhLuong = function(){
        if(this.chucVu == 1){
            return this.luongCoBan*3;
        }
        else if(this.chucVu == 2){
            return this.luongCoBan*2;
        }
        else{
            return this.luongCoBan;
        }
    }
    this.xepLoai = function(){
        if(this.gioLam >= 192){
            return 1;
        }
        else if(this.gioLam >=176){
            return 2;
        } else if(this.gioLam >=160){
            return 3;
        }
        else{
            return 4;
        }
    }
}
function layDuLieu(){
    var taiKhoan = document.getElementById("tknv").value ;
    var hoTen = document.getElementById("name").value ;
    var email = document.getElementById("email").value ;
    var matKhau = document.getElementById("password").value ;
    var ngayLam = document.getElementById("datepicker").value ;
    var luongCoBan = document.getElementById("luongCB").value*1 ;
    var chucVu = document.getElementById("chucvu").value ;
    var gioLam = document.getElementById("gioLam").value*1 ;
    var nv = new nhanVien(taiKhoan,hoTen,email,matKhau,ngayLam,luongCoBan,chucVu,gioLam);
    return nv;
}
function renderNV(arrNV){
    var contentHTML = "";
    for(var i = 0; i< arrNV.length; i++){
        var chucVu = "";
        var loaiNV = "";
        if(arrNV[i].xepLoai() == 1){
            loaiNV = "Xuất sắc";
        } else if(arrNV[i].xepLoai() == 2){
            loaiNV = "Giỏi";
        } else if(arrNV[i].xepLoai() == 3){
            loaiNV = "Khá";
        }
        else{
            loaiNV = "Trung Bình";
        }
        if(arrNV[i].chucVu == 1){
            chucVu = "Sếp";
        } else if(arrNV[i].chucVu == 2){
            chucVu = "Trưởng Phòng"
        }
        else{
            chucVu = "Nhân viên";
        }
        var contentTr = `
            <tr>
            <td>${arrNV[i].taiKhoan}</td>
            <td>${arrNV[i].hoTen}</td>
            <td>${arrNV[i].email}</td>
            <td>${arrNV[i].ngayLam}</td>
            <td>${chucVu}</td>
            <td>${arrNV[i].tinhLuong()}</td>
            <td>${loaiNV}</td>
            <td><button class="btn btn-danger" onclick="xoaNV(${arrNV[i].taiKhoan})">Xoá</button>
            <button class="btn btn-warning" id="btnSua" data-toggle="modal" data-target="#myModal" onclick="suaNV(${arrNV[i].taiKhoan})">Sửa</button>
            </td>
            </tr>
        `;
        contentHTML += contentTr;
    }
    document.getElementById("tableDanhSach").innerHTML = contentHTML;
}
function timViTri(id,dsnv){
    for(var i = 0; i < dsnv.length;i++){
        if(id == dsnv[i].taiKhoan){
            return i;
        }
        
    }
    return -1;
}
function showThongTinLenForm(NV){
    document.getElementById("tknv").value = NV.taiKhoan;
    document.getElementById("name").value = NV.hoTen;
    document.getElementById("email").value = NV.email;
    document.getElementById("password").value = NV.matKhau;
    document.getElementById("datepicker").value = NV.ngayLam;
    document.getElementById("luongCB").value = NV.luongCoBan;
    document.getElementById("chucvu").value = NV.chucVu;
    document.getElementById("gioLam").value = NV.gioLam;
}
function resetForm(){
    document.getElementById("QLNV-form").reset();
}
function locNhanVien(value,dsnv){
    var danhSach = [];
    if (value == 1){
        for (i = 0; i< dsnv.length;i++){
            if (dsnv[i].xepLoai()== 1){
                danhSach.push(dsnv[i])
            }
        }
    }
    else if (value == 2){
        for (i = 0; i< dsnv.length;i++){
            if (dsnv[i].xepLoai()== 2){
                danhSach.push(dsnv[i])
            }
        }
    }
     else if (value == 3){
        for (i = 0; i< dsnv.length;i++){
            if (dsnv[i].xepLoai()== 3){
                danhSach.push(dsnv[i])
            }
        }
    }
    else if (value == 4){
        for (i = 0; i< dsnv.length;i++){
            if (dsnv[i].xepLoai()== 4){
                danhSach.push(dsnv[i])
            }
        }
    }
    else{
        danhSach = dsnv;
    }
    return danhSach;
}
