var dsnv = [];
function luuLocalStorage(){
    let jsonDsnv = JSON.stringify(dsnv);
    localStorage.setItem("DSNV",jsonDsnv)
}
var dataJson = localStorage.getItem("DSNV");
console.log("dataJson: ",dataJson);
if(dataJson !== null){
    var svArr = JSON.parse(dataJson);

    for(var index = 0; index < svArr.length; index++){
        var item = svArr[index];
        var nv = new nhanVien(
            item.taiKhoan,
            item.hoTen,
            item.email,
            item.matKhau,
            item.ngayLam,
            item.luongCoBan,
            item.chucVu,
            item.gioLam,
        );
        dsnv.push(nv);
    }
    renderNV(dsnv);
}
function themNV(){
    document.getElementById("ketQua").innerHTML = "";
    var nv = layDuLieu();
    var isValid = true;
    isValid = kiemTraDoDai(nv.taiKhoan,"tbTKNV") && kiemTraTrung(nv.taiKhoan,"tbTKNV",dsnv) && kiemTraSo(nv.taiKhoan,"tbTKNV");
    isValid = isValid & (kiemTraChu(nv.hoTen,"tbTen") && kiemTraRong(nv.hoTen,"tbTen"));
    isValid = isValid & (kiemTraRong(nv.email,"tbEmail") && kiemTraEmail(nv.email,"tbEmail"));
    isValid = isValid & (kiemTraRong(nv.matKhau,"tbMatKhau") && kiemTraPass(nv.matKhau,"tbMatKhau"));
    isValid = isValid & (kiemTraRong(nv.ngayLam,"tbNgay") && kiemTraNgay(nv.ngayLam,"tbNgay"));
    isValid = isValid & (kiemTraRong(nv.luongCoBan,"tbLuongCB") && kiemTraLuong(nv.luongCoBan,"tbLuongCB"));
    isValid = isValid & kiemTraChucVu(nv.chucVu,"tbChucVu");
    isValid = isValid & kiemTraGioLam(nv.gioLam,"tbGiolam");
    if(isValid){
        dsnv.push(nv);
        document.getElementById("ketQua").innerHTML = "Thêm Nhân Viên Thành Công!"
        luuLocalStorage();
    }
    
    renderNV(dsnv);
}
function xoaNV(id){
    // console.log("id: ",id);
    var viTri = timViTri(id,dsnv);
    // console.log("vi tri: ",viTri);
    if (viTri !== -1){
        dsnv.splice(viTri,1);
        renderNV(dsnv);
    }
    luuLocalStorage();
}
function suaNV(id){
    document.getElementById("ketQua").innerHTML = "";
    var viTri = timViTri(id,dsnv);
//    console.log ("vi tri: ",viTri);
    if(viTri !== -1){
        showThongTinLenForm(dsnv[viTri])
    }
}
function capNhat(){
    document.getElementById("ketQua").innerHTML = "";
    var nv = layDuLieu();
    var viTri = timViTri(nv.taiKhoan,dsnv);
    var isValid = true;
    isValid = kiemTraDoDai(nv.taiKhoan,"tbTKNV") && kiemTraTrungCN(nv.taiKhoan,"tbTKNV",dsnv) && kiemTraSo(nv.taiKhoan,"tbTKNV");
    isValid = isValid & (kiemTraChu(nv.hoTen,"tbTen") && kiemTraRong(nv.hoTen,"tbTen"));
    isValid = isValid & (kiemTraRong(nv.email,"tbEmail") && kiemTraEmail(nv.email,"tbEmail"));
    isValid = isValid & (kiemTraRong(nv.matKhau,"tbMatKhau") && kiemTraPass(nv.matKhau,"tbMatKhau"));
    isValid = isValid & (kiemTraRong(nv.ngayLam,"tbNgay") && kiemTraNgay(nv.ngayLam,"tbNgay"));
    isValid = isValid & (kiemTraRong(nv.luongCoBan,"tbLuongCB") && kiemTraLuong(nv.luongCoBan,"tbLuongCB"));
    isValid = isValid & kiemTraChucVu(nv.chucVu,"tbChucVu");
    isValid = isValid & kiemTraGioLam(nv.gioLam,"tbGiolam");
    if (isValid){
        dsnv[viTri] = nv;
        document.getElementById("ketQua").innerHTML = "Cập Nhật Nhân Viên Thành Công!"
        renderNV(dsnv);
        luuLocalStorage();
        resetForm();
    }
}
function timNV(){
    // console.log("yes");
    var dsnvFilted = [];
    var value = document.getElementById("searchName").value;
    dsnvFilted = locNhanVien(value,dsnv);
    renderNV(dsnvFilted);
}