function showMassage(id,massage,display){
    if(display == 0){
        document.getElementById(id).style.display = "none";
    } else{
        document.getElementById(id).style.display = "block";
    }
    document.getElementById(id).innerHTML = massage;
}
function kiemTraDoDai(input,id){
    if (input.length >= 4 && input.length <=6){
        showMassage(id,"",0);
        return true;
    }
    else {
        showMassage(id,"Vui lòng nhập từ 4 - 6 chữ số",1);
        return false;
    }
}
function kiemTraTrung(input,idErr,dsnv){
    var viTri = timViTri(input,dsnv);
    if (viTri == -1){
        showMassage(idErr,"",0)
        return true;
    }
    else{
        showMassage(idErr,"Tài khoản đã tồn tại",1);
        return false;
    }
}
function kiemTraSo(input,idErr){
    var regex = /^\d+$/;
    let isNumber = regex.test(input);
    if(isNumber){
        showMassage(idErr,"",0)
        return true;
    }
    else{
        showMassage(idErr,"Vui lòng nhập số 0 - 9",1);
        return false;
    }
}
function kiemTraChu(input,idErr){
    var regex = /^[a-zA-Z\s]*$/;
    let isLetter = regex.test(input);
    if(isLetter){
        showMassage(idErr,"",0)
        return true;
    }
    else{
        showMassage(idErr,"Vui lòng nhập chữ",1);
        return false;
    }
}
function kiemTraRong(input,idErr){
    if(input.length == 0){
        showMassage(idErr,"Vui lòng không để trống",1);
        return false;
    }
    else{
        showMassage(idErr,"",0)
        return true;
    }
}
function kiemTraEmail(input,idErr){
    var regex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    let isEmail = regex.test(input);
    if(isEmail){
        showMassage(idErr,"",0)
        return true;
    }
    else{
        showMassage(idErr,"Vui lòng nhập đúng định dạng email abc@domain.com",1);
        return false;
    }
}
function kiemTraPass(input,idErr){
    var regex = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,10}$/;
    let isPass = regex.test(input);
    if(isPass){
        showMassage(idErr,"",0)
        return true;
    }
    else{
        showMassage(idErr,"Vui lòng nhập 6-10 kí tự, 1 chữ hoa, 1 số, 1 symbol",1);
        return false;
    }
}
function kiemTraNgay(input,idErr){
    var regex = /^((0?[1-9]|1[012])[- /.](0?[1-9]|[12][0-9]|3[01])[- /.](19|20)?[0-9]{2})*$/;
    let isDate = regex.test(input);
    if(isDate){
        showMassage(idErr,"",0)
        return true;
    }
    else{
        showMassage(idErr,"Vui lòng nhập đúng mm/dd/yyyy",1);
        return false;
    }
}
function kiemTraLuong(input,idErr){
    if( input >= 1000000 && input <= 20000000){
        showMassage(idErr,"",0)
        return true;
    }
    else{
        showMassage(idErr,"Vui lòng nhập lương từ 1 000 000 - 20 000 000",1);
        return false;
    }
}
function kiemTraChucVu(input,idErr){
    if(input == 1 || input == 2 || input == 3){
        showMassage(idErr,"",0)
        return true;
    }
    else{
        showMassage(idErr,"Vui lòng chọn chức vụ",1);
        return false;
    }
}
function kiemTraGioLam(input,idErr){
    if (input >= 80 && input <= 200){
        showMassage(idErr,"",0)
        return true;
    }
    else{
        showMassage(idErr,"Giờ công phải từ 80 - 200",1);
        return false;
    }
}
function kiemTraTrungCN(input,idErr,dsnv){
    var viTri = timViTri(input,dsnv);
    if (viTri == -1){
        showMassage(idErr,"Tài khoản Không tồn tại",1);
        return false;
    }
    else{
        showMassage(idErr,"",0);
        return true;
    }
}